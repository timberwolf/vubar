CFLAGS+=-g
LFLAGS+=-lpulse -lpulse-simple -lm

vubar: *.c
	cc ${CFLAGS} -o vubar *.c ${LFLAGS}

clean:
	rm vubar
