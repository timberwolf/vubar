# vubar

A simple VU bar in your terminal!

## Compilation

You'll need the PulseAudio development files. On Ubuntu,
you should be able to get these through apt:
```
sudo apt install libpulse-dev
```

Then just run `make` to build the program.

## Usage

```
vubar [--source source] [--channels n]
```

and a bunch more! See `vubar --help` for a full list.

vubar can take a PulseAudio source name to listen to; if you want to
monitor an output, append `.monitor` to the output sink name. You can get
the available outputs with this command:

```
pacmd list-sinks | grep 'name:' | sed -Ee 's/.*<(.*)>.*/\1.monitor/'
```

If no source is given, it defaults to monitoring the audio playback.

## License

This is free and unencumbered software released into the public domain.
See [COPYING.md](COPYING.md) for more information.
