#include <stdio.h>
#include <pulse/simple.h>
#include <pulse/error.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include <math.h>
#include <unistd.h>
#include <getopt.h>

int main (int argc, char **argv) {
	char *source = NULL;
	bool sourceStringMalloced = 0;
	int channels = 1;
	bool stacked = 0;
	int vert = 0; // vertical height, or 0 if not in vertical mode
	char *barChar = "#";
	char *spaceChar = "-";
	
	{
		// option parsing
		struct option options[] = {
			{"source",   required_argument, NULL, 'M'},
			{"channels", required_argument, NULL, 'c'},
			{"stacked",  no_argument, NULL,       'S'},
			{"vert",     required_argument, NULL, 'V'},
			{"help",     no_argument, NULL,       'h'},
			{0, 0, 0, 0}
		};
		
		char c;
		while ((c = getopt_long(argc, argv, "c:sV:B:S:h", options, NULL)) != -1) {
			switch (c) {
				case 'M':
					// source
					// not a short option!
					printf("optarg: %s / sizeof(optarg): %lu\n", optarg, (strlen(optarg) + 1) * sizeof(char));
					source = malloc((strlen(optarg) + 1) * sizeof(char));
					sourceStringMalloced = 1;
					strcpy(source, optarg);
					printf("[set source from argument]\n");
					break;
				case 'c':
					channels = atoi(optarg);
					break;
				case 's':
					stacked = 1;
					break;
				case 'V':
					vert = atoi(optarg);
					break;
				case 'B':
					barChar = malloc((strlen(optarg) + 1) * sizeof(char));
					strcpy(barChar, optarg);
					break;
				case 'S':
					spaceChar = malloc((strlen(optarg) + 1) * sizeof(char));
					strcpy(spaceChar, optarg);
					break;
				case 'h':
					printf("Usage: vubar [options]\n"
						"\t--source \e[4msource\e[0m: pick the PulseAudio source to monitor\n"
						"\t\tMonitor a sink with '\e[4msink\e[0m.monitor'\n"
						"\t\tDefaults to monitoring the default sink (i.e. audio playback)\n"
						"\t-c/--channels \e[4mn\e[0m: set the number of channels (e.g. 2 for stereo)\n"
						"\t\tDefaults to 1, average volume of all channels\n"
						"\t-s/--stacked: stack multiple channel bars instead of side-by-side\n"
						"\t-V/--vert \e[4mn\e[0m: vertical mode, n rows high\n"
						"\t-B \e[4mc\e[0m: set the bar character\n"
						"\t-S \e[4mc\e[0m: set the space character\n"
						"\t-h/--help: display this help\n"
					);
					return 0;
				case '?':
					fprintf(stderr, "Error in option parsing - either unknown option or missing argument\n");
					return 1;
				default:
					fprintf(stderr, "Error in option parsing! - got option code %c\n", c);
					return 1;
			}
		}
	}
	
	if (source == NULL) {
		// get the default sink
		FILE *pa_info_stream = popen("pactl info | grep \"Default Sink:\" | perl -pe \"s/.*?: (.*)/\\1.monitor/\" | tr -d '\n'", "r");
		char info_output[4096];
		fgets(info_output, 4096, pa_info_stream);
		pclose(pa_info_stream);
		source = malloc(sizeof(info_output));
		sourceStringMalloced = 1;
		strcpy(source, info_output);
	}
	// let you use the default source with "default"
	printf("Using source: %s\n", source);
	if (strcasecmp(source, "default") == 0) {
		if (sourceStringMalloced) {
			free(source);
			sourceStringMalloced = 0;
		}
		source = NULL;
	}
	
	// set up PulseAudio
	
	pa_sample_spec sampleSpec = {PA_SAMPLE_S16LE, 44100, channels};
	pa_channel_map channelMap;
	pa_channel_map_init_auto(&channelMap, channels, PA_CHANNEL_MAP_DEFAULT);
	int errorID = 0;
	pa_simple *pulseaudio = pa_simple_new(
		NULL,
		"vubar",
		PA_STREAM_RECORD,
		source,
		"Audio monitoring",
		&sampleSpec,
		&channelMap,
		NULL,
		&errorID
	);
	if (pulseaudio == NULL) {
		fprintf(stderr, "Error setting up PulseAudio: %s\n", pa_strerror(errorID));
		return 1;
	}
	
	if (sourceStringMalloced) {
		free(source);
		source = NULL;
	}
	
	printf("Connected to PulseAudio.\n");
	
	if (vert) {
		// make space for the channel bars, since the first thing it'll do is go up to the top of the gap
		for (int i = 0; i < vert-1; i++) {
			printf("\n");
		}
	} else if (stacked) {
		for (int i = 0; i < channels - 1; i++) {
			printf("\n");
		}
	}
	
	int16_t audioBuffer[256];
	while (1) {
		int errorID = 0;
		pa_simple_read(pulseaudio, &audioBuffer, sizeof(audioBuffer), &errorID);
		if (errorID > 0) {
			fprintf(stderr, "Error reading data: %s\n", pa_strerror(errorID));
			return 2;
		}
		
		printf("\r");
		if (vert) {
			// move cursor to top of the drawing area
			for (int i = 0; i < vert-1; i++) {
				printf("\e[F");
			}
		} else if (stacked && channels > 1) {
			// move cursor to top of the list of bars
			for (int i = 0; i < channels - 1; i++) {
				printf("\e[F");
			}
		}
		
		for (int channelIndex = 0; channelIndex < channels; channelIndex++) {
			// find the audio power level, RMS
			// data: channels are interleaved!
			long int rms = 0;
			int nDataPoints = sizeof(audioBuffer) / sizeof(int16_t);
			nDataPoints = nDataPoints / channels;
			// sum of squares
			for (int i = channelIndex; i < nDataPoints; i+=channels) {
				rms += ((long int)audioBuffer[i] * audioBuffer[i]);
			}
			// mean
			rms /= nDataPoints;
			// root
			rms = (long int)sqrt((double)rms);
			
			double rms_normalized = (double)rms / (INT16_MAX/2);
			// printf("%ld\n", rms);
			// printf("%f\n", rms_normalized);
			
			// clip meter just in case
			if (rms_normalized > 1.0) {
				rms_normalized = 1.0;
			}
			
			if (vert) {
				// move vertically up, if needed
				if (channelIndex > 0) {
					for (int i = 0; i < vert-1; i++) {
						printf("\e[F");
					}
				}
				// move horizontally to bar position
				for (int i = 0; i < channelIndex * 3; i++) {
					printf("\e[C");
				}
				for (int i = 0; i < vert; i++) {
					if ((vert - i) < rms_normalized * vert) {
						printf("%s%s", barChar, barChar);
					} else {
						printf("%s%s", spaceChar, spaceChar);
					}
					if (i < vert-1) {
						printf("\e[D\e[D\e[B");
					}
				}
			} else {
				struct winsize winsize;
				ioctl(STDOUT_FILENO, TIOCGWINSZ, &winsize);
				int cols = winsize.ws_col;
				int barwidth;
				if (stacked) {
					barwidth = cols - 2;
				} else {
					barwidth = (cols / channels) - 2;
				}
				
				int i;
				printf(" ");
				for (i = 0; i < rms_normalized * barwidth; i++) {
					printf("%s", barChar);
				}
				for (; i < barwidth; i++) {
					printf("%s", spaceChar);
				}
				printf(" ");
				printf("\e[K");
			}
		}
		fflush(stdout);
	}
	
	pa_simple_free(pulseaudio);
}
